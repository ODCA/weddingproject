package app.recipe;

import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.*;

public class RecipeController {

    @FXML
    private TextField searchField;

    @FXML
    private CheckBox starter;

    @FXML
    private CheckBox mainCourse;

    @FXML
    private CheckBox desert;

    @FXML
    private CheckBox drink;

    @FXML
    private VBox recipeContainer;

    private RecipeModel model;
    private Map<String,RecipeItemController> recipeControllers;




    public void initController() throws IOException{
        System.out.println("init controller");
        model = new RecipeModel();
        recipeControllers = new HashMap<>();
        List<Recipe> recipes = model.update();
        FXMLLoader loader;

        for(Recipe r : recipes) {
            loader = new FXMLLoader(getClass().getResource("/recipe/RecipeItem.fxml"));
            Parent recipeItem = loader.load();
            RecipeItemController itemController = loader.getController();
            itemController.init(model, r, this);
            recipeControllers.put(r.getName(), itemController);

            recipeContainer.getChildren().add(recipeItem);
        }
    }

    public void updateList() {
        recipeContainer.getChildren().clear();

        List<Recipe> recipes = model.update();

        for (Recipe r : recipes) {
            recipeContainer.getChildren().add(recipeControllers.get(r.getName()).root);
            if (!model.selected(r)) {
                recipeControllers.get(r.getName()).deselect();
            }
        }
    }

    @FXML
    private void addRecipe(final ActionEvent event) {
        //create new window that can create new
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/recipe/CreateRecipe.fxml"));
        try {
            Parent addPane;
            addPane = loader.load();
            CreateRecipeController createController = loader.getController();
            Stage popupStage = new Stage();
            createController.init(popupStage, model);

            Scene scene = new Scene(addPane, 600, 400);
            popupStage.setScene(scene);
            popupStage.initModality(Modality.APPLICATION_MODAL);
            popupStage.showAndWait();

            updateList();

        } catch (IOException e) {
            e.printStackTrace();
        }

        event.consume();
    }

    @FXML
    private void textSearch(final ActionEvent event) {
        System.out.println("filter name");
        model.filterOnName(searchField.getText());
        updateList();

        event.consume();
    }

    @FXML
    private void typeSearch(final ActionEvent e) {
        System.out.println("filter food type");
        List<FoodType> foodType = new LinkedList<>();
        if(starter.isSelected()) {
            foodType.add(FoodType.STARTER);
        }
        if (mainCourse.isSelected()) {
            foodType.add(FoodType.MAINCOURSE);
        }
        if (desert.isSelected()) {
            foodType.add(FoodType.DESERT);
        }
        if (drink.isSelected()) {
            foodType.add(FoodType.DRINK);
        }

        model.filterOnType(foodType);

        updateList();

        e.consume();
    }
}

