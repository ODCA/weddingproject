package app.recipe;

import app.HostServicesProvider;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Optional;

public class RecipeItemController {

    @FXML
    private Label title;

    @FXML
    public AnchorPane root;

    @FXML
    private Label description;

    @FXML
    private Hyperlink url;

    @FXML
    private Button editButton;

    @FXML
    private Button deleteButton;

    private RecipeModel model;
    private Recipe recipe;
    private RecipeController parentController;

    void init(RecipeModel model, Recipe recipe, RecipeController parentController) {
        title.setText(recipe.getName());
        description.setText(recipe.getDescription());
        url.setText(recipe.getUrl());
        this.model = model;
        this.recipe = recipe;
        this.parentController = parentController;
    }

    void deselect() {
        root.setStyle("-fx-background-color: #FFFFFF");
        editButton.setVisible(false);
        deleteButton.setVisible(false);

    }

    @FXML
    private void selected() {
        System.out.println("Selected Item");
        this.model.selectRecipe(recipe.getName());
        root.setStyle("-fx-background-color: #C0C0C0");
        editButton.setVisible(true);
        deleteButton.setVisible(true);
        parentController.updateList();
    }

    @FXML
    private void removeRecipe() {
        Alert confirmation = new Alert(Alert.AlertType.CONFIRMATION);
        confirmation.setTitle("Bekräftelse");
        confirmation.setHeaderText("Vill du verkligen ta bort receptet");

        Optional<ButtonType> result = confirmation.showAndWait();
        if(result.get() == ButtonType.OK) {
            model.remove();
            parentController.updateList();
        }
    }

    @FXML
    private void editRecipe() {
        System.out.println("edit recipe");
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/recipe/CreateRecipe.fxml"));

        try {

            Parent pane = loader.load();
            CreateRecipeController createRecipeController = loader.getController();
            Stage stage = new Stage();
            Scene scene = new Scene(pane, 600, 400);
            stage.setScene(scene);
            stage.initModality(Modality.APPLICATION_MODAL);
            createRecipeController.init(stage, model, recipe);
            stage.showAndWait();

            recipe = createRecipeController.getRecipe();

            title.setText(recipe.getName());
            description.setText(recipe.getDescription());
            url.setText(recipe.getUrl());
            parentController.updateList();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void openURL() {
        System.out.println("Open Webbrowser");
        HostServicesProvider.INSTANCE.getHostServices().showDocument(recipe.getUrl());
//        final WebView browser = new WebView();
//        final WebEngine webEngine = browser.getEngine();
//        webEngine.load(recipe.getUrl());
    }
}
