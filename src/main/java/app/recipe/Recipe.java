package app.recipe;

public class Recipe {
    private String name, description, url;
    public final FoodType foodType;

    public Recipe(FoodType foodType, String name, String description) {
        this.foodType = foodType;
        this.name = name;
        this.description = description;
    }

    public Recipe(FoodType foodType, String name, String description, String url) {
        this.foodType = foodType;
        this.name = name;
        this.description = description;
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getUrl() {
        return url;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;

        if (o instanceof String) {
            String name = (String) o;
            if (name.toLowerCase().equals(this.name.toLowerCase()))
                return true;
            else
                return false;
        }

        if (!( o instanceof Recipe))
            return false;

        Recipe r = (Recipe) o;
        if (name.toLowerCase().equals(r.getName().toLowerCase()))
            return true;

        return false;
    }
}
