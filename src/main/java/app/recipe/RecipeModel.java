package app.recipe;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.*;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class RecipeModel {

    private List<Recipe> recipes;
    private RecipeCollection recipeCollection;
    private List<FoodType> foodTypeFilter;
    private String searchWord;
    private Gson gson;
    private File saveFile;
//    private PropertyChangeSupport pcs;
    private Recipe currentRecipe;

    public RecipeModel() throws IOException {
        gson = new GsonBuilder().setPrettyPrinting().create();

        try {
            saveFile = new File("recipes.json");
            readFromSaveFile();
        } catch (FileNotFoundException e) {
            System.out.println("File not found, Create new empty collection");
            saveFile = new File("recipes.json");
            saveFile.createNewFile();
            this.recipeCollection = new RecipeCollection();
        }
        recipes = recipeCollection.getAll();
        foodTypeFilter = new LinkedList<>();
        searchWord = "";
//        pcs = new PropertyChangeSupport(this);
    }

    public RecipeModel(RecipeCollection collection) throws IOException {
        this.recipeCollection = collection;
        recipes = recipeCollection.getAll();
        foodTypeFilter = new LinkedList<>();
        searchWord = "";
        gson = new GsonBuilder().setPrettyPrinting().create();
        saveFile = new File("recipes.json");
        saveFile.createNewFile();
//        pcs = new PropertyChangeSupport(this);
    }

    public List<Recipe> update() {
        recipes = recipeCollection.getAll();
        //filter on type
        recipes = removeOnName(recipes);

        //filter on search
        recipes = removeOnType(recipes);

        //support.firePropertyChange("list", null, recipes);
        return recipes;
    }

    public void add(Recipe recipe) {
        recipeCollection.addRecipe(recipe);
        saveToSaveFile();
        update();
    }

    public void remove() {
        recipeCollection.removeRecipe(currentRecipe.getName());
        saveToSaveFile();
        update();
    }

    public void filterOnType(List<FoodType> filter) {
        System.out.println(filter.toString());
        foodTypeFilter = filter;
        update();
    }

    public void filterOnName(String search) {
        searchWord = search;
        update();
    }

    public void selectRecipe(String recipe) {
        System.out.println(recipe);
        currentRecipe = recipeCollection.getRecipe(recipe);

    }

    public boolean selected(Recipe recipe) {
        return recipe.equals(currentRecipe);
    }

    private List<Recipe> removeOnName(List<Recipe> recipeList) {
        return recipeList.stream()
                .filter(r -> r.getName().toLowerCase()
                        .contains(searchWord.toLowerCase()))
                .collect(Collectors.toList());
    }

    private List<Recipe> removeOnType(List<Recipe> recipeList) {
        return recipeList.stream().filter(r -> foodTypeFilter.contains(r.foodType) || foodTypeFilter.size() == 0).collect(Collectors.toList());
    }

    private void saveToSaveFile() {
        System.out.println("Save to file:" + saveFile.getPath());
        try {
            Writer writer = new FileWriter(saveFile.getPath());
            gson.toJson(recipeCollection, writer);
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void readFromSaveFile() throws FileNotFoundException {
        recipeCollection = gson.fromJson(new FileReader(saveFile.getPath()), RecipeCollection.class);
    }
}
