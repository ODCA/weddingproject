package app.recipe;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

import java.util.Optional;


public class CreateRecipeController {
    private Recipe recipe = null;
    private Stage stage;
    private  RecipeModel model;

    public void init(Stage stage, RecipeModel model) {
        this.stage = stage;
        this.model = model;
    }

    public void init(Stage stage, RecipeModel model, Recipe recipe) {
        this.stage = stage;
        this.model = model;
        this.recipe = recipe;
        titleField.setText(recipe.getName());
        descField.setText(recipe.getDescription());
        if (recipe.getUrl() == null) {
            urlField.setText("");
        }
        else {
            urlField.setText(recipe.getUrl());
        }

        switch (recipe.foodType) {
            case STARTER:
                starter.setSelected(true);
                break;
            case MAINCOURSE:
                mainCourse.setSelected(true);
                break;
            case DESERT:
                desert.setSelected(true);
                break;
            case DRINK:
                drink.setSelected(true);
                break;
        }
    }

    public Recipe getRecipe() {
        return recipe;
    }

    @FXML
    private TextField titleField;

    @FXML
    private TextField descField;

    @FXML
    private TextField urlField;

    @FXML
    private RadioButton starter;

    @FXML
    private RadioButton mainCourse;

    @FXML
    private RadioButton desert;

    @FXML
    private RadioButton drink;

    @FXML
    private void saveRecipe() {
        System.out.println("save");
        saveNewRecipe();
    }

    @FXML
    private void  abort() {
        System.out.println("abort");
        boolean abort = closeStageAbort();

        if (abort) {
            stage.close();
        }
    }

    @FXML
    private void onKeyPressed(KeyEvent event) {
        if (event.getCode() == KeyCode.ENTER) {
           saveRecipe();
        }
    }

    private void saveNewRecipe() {
        FoodType foodType;
        if(starter.isSelected()) {
            foodType = FoodType.STARTER;
        }

        else if (mainCourse.isSelected()) {
            foodType = FoodType.MAINCOURSE;
        }

        else if (desert.isSelected()) {
            foodType = FoodType.DESERT;
        }
        else {
            foodType = FoodType.DRINK;
        }

        if (urlField.getText().equals("")) {
            recipe = new Recipe(foodType,titleField.getText(), descField.getText());
        }

        else {
            recipe = new Recipe(foodType,titleField.getText(), descField.getText(), urlField.getText());
        }

        boolean save = closeStageSave();
        if(save) {
            model.add(recipe);
            stage.close();
        }
        else {
            recipe = null;
        }
    }

    private boolean closeStageSave() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Spara");
        alert.setHeaderText("Vill du spara receptet");

        Optional<ButtonType> result = alert.showAndWait();
        if(result.get() == ButtonType.OK) {
            System.out.println("Save Recipe");
            return true;
        }

        else {
            System.out.println("Do not save, go back");
            return false;
        }
    }

    private boolean closeStageAbort() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Avsluta");
        alert.setHeaderText("Vill du avsluta utan att skapa nytt recept");

        Optional<ButtonType> result = alert.showAndWait();
        if(result.get() == ButtonType.OK) {
            System.out.println("abort ");
            return true;
        }

        else {
            System.out.println("go back");
            return false;
        }
    }
}
