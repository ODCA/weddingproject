package app.recipe;

import java.util.*;


public class RecipeCollection {
    private Map<String, Recipe> recipeMap;

    public RecipeCollection() {
        recipeMap = new HashMap<>();
    }

    public int size() {
        return recipeMap.size();
    }

    public Recipe addRecipe(Recipe recipe) {

        return recipeMap.put(recipe.getName(), recipe);
    }

    public List<Recipe> getAll() {
        return new ArrayList<>(recipeMap.values());
    }

    public Recipe removeRecipe(String name) {
        return recipeMap.remove(name);
    }

    public Recipe getRecipe(String name) {
        return recipeMap.get(name);
    }
}
