package app.task;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class UsersList implements Iterable<User> {

    private List<User> users;

    public UsersList() {
        users = new ArrayList<>();
    }

    public boolean add(User u) {
        for (User user : users) {
            if(user.name().equals(u.name())) {
                return false;
            }
        }
        return users.add(u);
    }

    public int size() {
        return users.size();
    }

    public User get(String s) {
        for (int i = 0; i < users.size(); i++) {
            if(users.get(i).name().equals(s)) {
                return users.get(i);
            }
        }

        return null;
    }

    public User remove(String s) {
        for (int i = 0; i < users.size(); i++) {
            if(users.get(i).name().equals(s)) {
                return users.remove(i);
            }
        }

        return null;
    }

    public boolean remove(User u) {
        return users.remove(u);
    }


    @Override
    public Iterator<User> iterator() {
        return users.iterator();
    }
}
