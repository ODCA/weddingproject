package app.task;


import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ListView;
import javafx.scene.control.TextInputDialog;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class TaskDistributorController {


    private TaskModel model;
    @FXML
    private ListView<String> TaskList;

    @FXML
    private ListView<String> ToDoList;

    public void init() {
        model = new TaskModel();
        updateListViews();
    }

    @FXML
    void distributeTask(ActionEvent event) {
        Map<String, List<String>> distribution = model.distribute();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/task/ViewDistribution.fxml"));
        Parent viewDistribution;
        try {
            viewDistribution = loader.load();
            Stage popupStage = new Stage();
            ViewDistributionController controller = loader.getController();
            controller.init(distribution, popupStage);

            Scene scene = new Scene(viewDistribution);
            popupStage.setScene(scene);
            popupStage.initModality(Modality.APPLICATION_MODAL);
            popupStage.showAndWait();

        } catch (IOException e) {
            e.printStackTrace();
        }
        updateListViews();
    }

    @FXML
    void editUsers(ActionEvent event) {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/task/ConfigureUsers.fxml"));

        Parent usersPane;
        try {
            usersPane = loader.load();
            //ConfigureUsersController controller = loader.getController();
            Stage popupStage = new Stage();
            ConfigureUsersController controller = loader.getController();
            controller.init(model, popupStage);

            Scene scene = new Scene(usersPane);
            popupStage.setScene(scene);
            popupStage.initModality(Modality.APPLICATION_MODAL);
            popupStage.showAndWait();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    void newTask(ActionEvent event) {
        TextInputDialog dialog = new TextInputDialog();
        dialog.setTitle("Ny Uppgift");
        dialog.setHeaderText("Skriv in namnet på den nya uppgiften");
        dialog.setContentText("Namn på uppgift");

        Optional<String> task = dialog.showAndWait();
        if(task.isPresent()) {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Ny uppgift");
            alert.setHeaderText("ny uppgift " + task.get() + " har skapats");
            Optional<ButtonType> result = alert.showAndWait();
            if(result.get() == ButtonType.OK) {
                model.createTask(task.get());
                updateListViews();
            }
        }
    }

    @FXML
    void removeTask(ActionEvent event) {
        ObservableList<String> taskList = TaskList.getSelectionModel().getSelectedItems();
        if(!taskList.isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Ta bort uppgifter");
            alert.setHeaderText("Är du säker på att du vill ta bort " + taskList.size() + " uppgifter?");
            Optional<ButtonType> result = alert.showAndWait();

            if(result.get() == ButtonType.OK) {
                for(String task : taskList) {
                    model.removeTask(task);
                }
            }
        }
        updateListViews();
    }

    @FXML
    void moveToToDo() {
        ObservableList<String> selectedTasks = TaskList.getSelectionModel().getSelectedItems();
        if(!selectedTasks.isEmpty()) {
            for(String task : selectedTasks) {
                model.addToDo(task);

            }
            updateListViews();
        }
    }

    @FXML
    void moveFromToDo() {
        ObservableList<String> selectedTask = ToDoList.getSelectionModel().getSelectedItems();
        if(!selectedTask.isEmpty()) {
            for(String task : selectedTask) {
                model.removeToDo(task);
            }
            updateListViews();
        }

    }

    private void updateListViews() {
        TaskList.getItems().clear();
        ToDoList.getItems().clear();
        TaskList.getItems().addAll(model.getTasks());
        ToDoList.getItems().addAll(model.getToDoList());
    }



}
