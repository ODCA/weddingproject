package app.task;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.stage.Stage;

import java.util.List;
import java.util.Optional;

public class ConfigureUsersController {

    @FXML
    private TextField NameField;

    @FXML
    private ListView userListView;

    private TaskModel model;
    private Stage stage;

    public void init(TaskModel model, Stage stage) {
        this.model = model;
        this.stage = stage;
        listUsers();
    }

    @FXML
    private void addUser(ActionEvent event) {
        if(NameField.getText().isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setHeaderText("Inget namn har skrivits");
            alert.setTitle("Inget Namn");
            alert.showAndWait();
        } else {
            model.createUser(NameField.getText());
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Ny användare");
            alert.setHeaderText("Ny användare har skapats");
            alert.showAndWait();
            listUsers();
            NameField.setText("");
        }
    }

    @FXML
    private void done(ActionEvent event) {
        model.saveState();
        stage.close();
    }

    @FXML
    private void removeUser(ActionEvent event) {
        ObservableList selectedItems = userListView.getSelectionModel().getSelectedItems();

        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Ta bort Användare");
        alert.setHeaderText("Är du säker på att du vill ta bort " + selectedItems.size() + " personer?");
        Optional<ButtonType> result = alert.showAndWait();

        if (result.get() == ButtonType.OK) {
            for(Object o : selectedItems) {
                if(o instanceof String) {
                    String name = (String) o;
                    model.removeUser(name);
                }
            }
        }
        listUsers();
    }

    private void listUsers() {
        List<String> users = model.getUsers();
        userListView.getItems().clear();
        userListView.getItems().addAll(users);
    }

}
