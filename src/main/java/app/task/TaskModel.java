package app.task;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.*;
import java.util.*;

public class TaskModel {

    //TODO: Create task specific class and list
    //TODO: new Way to call add task that is not string specific, same for users
    private List<String> tasks;
    private UsersList users;
    private List<String> taskToDo;
    private Gson gson;
    private File userSaveFile;
    private File taskSaveFile;

    public TaskModel() {
        tasks = new ArrayList<>();
        users = new UsersList();
        taskToDo = new ArrayList<>();
        gson = new GsonBuilder().setPrettyPrinting().create();
        userSaveFile = new File("users.json");
        taskSaveFile = new File("tasks.json");
        try {
            loadState();
        } catch (FileNotFoundException e) {
            try {
                userSaveFile.createNewFile();
                this.users = new UsersList();
            } catch (IOException ioE) {
                ioE.printStackTrace();
            }
        }
    }

    public int nbrTasks() {
        return tasks.size();
    }

    public boolean createTask(String task) {
        saveState();
        return tasks.add(task);
    }

    public boolean removeTask(String task) {
        saveState();
        return tasks.remove(task);
    }

    public int nbrUsers() {
        return users.size();
    }

    public User createUser(String name) {
        User u = new User(name);
        if (users.add(u)){
            saveState();
            return u;
        }
        else
            return null;
    }

    public User removeUser(String name) {
        saveState();
        return users.remove(name);
    }

    public int addWork(String name, String task) {
        if(users.get(name) != null) {
            return users.get(name).addWork(task);
        }
        return -1;
    }

    public int checkWork(String name, String task) {
        if(users.get(name) != null) {
            return users.get(name).checkWork(task);
        }
        return -1;
    }

    public boolean addToDo(String task) {
        if(!taskToDo.contains(task))
            return taskToDo.add(task);
        else {
            return false;
        }
    }

    public boolean removeToDo(String task) {
        if(taskToDo.contains(task))
            return taskToDo.remove(task);
        else {
            return false;
        }
    }

    public Map<String, List<String>> distribute() {
        //list that contains username and the task the user should do
        Map<String, List<String>> distribution = splitTasks();

        //clear list for new distribution
        taskToDo.clear();

        //add work for user
        for(User user : users) {
            List<String> taskForUser = distribution.get(user.name());
            for(String task : taskForUser) {
                user.addWork(task);
            }
        }
        saveState();

        return distribution;
    }

    public List<String> getUsers() {
        List<String> usernames = new LinkedList<>();

        for(User user : users) {
            usernames.add(user.name());
        }
        return usernames;
    }

    /**
     * saves current state to JSON file
     */
    public void saveState() {
        try {
            Writer userWriter = new FileWriter(userSaveFile.getPath());
            gson.toJson(users, userWriter);
            userWriter.flush();
            userWriter.close();
            Writer taskWriter = new FileWriter(taskSaveFile.getPath());
            gson.toJson(tasks, taskWriter);
            taskWriter.flush();
            taskWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    private void loadState() throws FileNotFoundException{
        users = gson.fromJson(new FileReader(userSaveFile.getPath()), UsersList.class);
        tasks = gson.fromJson(new FileReader(taskSaveFile.getPath()), LinkedList.class);
    }

    public List<String> getTasks() {
        return tasks;

    }

    public List<String> getToDoList() {
        return taskToDo;
    }

    private Map<String, List<String>> splitTasks() {
        //list that contains username and the task the user should do
        Map<String, List<String>> distribution = new HashMap<>();

        for (User user : users) {
            distribution.put(user.name(),new LinkedList<>());
        }
        Map<String, List<User>> rankedUsers = new HashMap<>();

        for (String task : taskToDo) {
            rankedUsers.put(task, rankUsers(task));
        }

        Iterator<String> itr = taskToDo.iterator();

        int mostTask = 0;
        while(itr.hasNext()) {
            String task = itr.next();
            boolean added = false;
            Iterator<User> rankedUserIterator = rankedUsers.get(task).iterator();
            for(User user : rankedUsers.get(task)) {
                if(distribution.get(user.name()).size() < mostTask) {
                    distribution.get(user.name()).add(task);
                    added = true;
                    break;
                }
            }
            if(!added) {
                distribution.get(rankedUsers.get(task).get(0).name()).add(task);
                mostTask++;
            }
        }
        return distribution;
    }

    private List<User> rankUsers(String task) {
        List<User> rankedList = new ArrayList<>();

        Iterator<User> itr =  users.iterator();
        if (!itr.hasNext()) {
            return null;
        }

        User user = itr.next();
        rankedList.add(user);
        while (itr.hasNext()) {
            user = itr.next();
            for(int i = 0; i < rankedList.size(); i++) {
                if(user.checkWork(task) < rankedList.get(i).checkWork(task) ) {
                    rankedList.add(i, user);
                    break;
                }
                else if(i == rankedList.size() - 1) {
                    rankedList.add(user);
                    break;
                }
            }
        }
        return rankedList;
    }
}
