package app.task;

import java.util.HashMap;
import java.util.Map;

public class User {

    private String name;
    private Map<String, Integer> tasksDone;

    public User(String name) {
        this.name = name;
        tasksDone = new HashMap<>();
    }

    String name() {
        return name;
    }

    int addWork(String task) {
        if (tasksDone.containsKey(task)) {
            tasksDone.put(task, tasksDone.get(task) + 1);
            return tasksDone.get(task) + 1;


        } else {
            tasksDone.put(task, 1);
            return 1;

        }
    }

    int checkWork(String task) {
        if (!tasksDone.containsKey(task)) {
            tasksDone.put(task, 0);
        }
        return tasksDone.get(task);
    }
}
