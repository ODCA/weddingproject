package app.task;

import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;

import java.util.List;
import java.util.Map;

public class ViewDistributionController {

    @FXML
    private HBox userHbox;

    private Stage stage;

    public void init(Map<String, List<String>> distribution, Stage stage) {
        this.stage = stage;
        for (String user : distribution.keySet()) {
            userHbox.getChildren().add(setUpUserView(user,distribution.get(user)));
        }
    }

    @FXML
    private void close() {
        stage.close();
    }

    private Node setUpUserView(String user, List<String> tasks) {
        VBox userView = new VBox();
        ListView listView = new ListView();
        Label name = new Label();

        userView.getChildren().add(listView);
        userView.getChildren().add(name);

        name.setText(user);
        name.setFont(new Font("System", 20));
        listView.setPrefHeight(300);
        for(String task : tasks) {
            listView.getItems().add(task);
        }

        userView.setSpacing(10);
        userView.setPadding(new Insets(5));
        return userView;
    }
}
