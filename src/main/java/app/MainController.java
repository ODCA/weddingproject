package app;

import app.recipe.RecipeController;
import app.task.TaskDistributorController;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;

public class MainController {

    /**
     * TODO: Make it possible to reset state w/o
     *  being forced to remove save files
     */

    private MainModel mainModel;

    @FXML
    private Button taskDistribution;
    @FXML
    private Button recipeCollection;


    @FXML
    private void loadTask(final ActionEvent event) throws Exception {
        System.out.println("Task clicked");
        HBox root = (HBox) taskDistribution.getScene().getRoot();
        if (root != null) {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/task/TaskDistributor.fxml"));
            Parent newPane = loader.load();

            //MainController controller = loader.getController();
            if (root.getChildren().size() > 1) {
                root.getChildren().remove(1,2);
            }

            root.getChildren().add(newPane);
            TaskDistributorController controller = loader.getController();
            controller.init();
        }
        event.consume();
    }

    @FXML
    protected void loadRecipe(final ActionEvent event)  throws Exception {
        System.out.println("Recipe clicked");
        HBox root = (HBox) recipeCollection.getScene().getRoot();
        if (root != null) {

            FXMLLoader loader = new FXMLLoader(getClass().getResource("/recipe/Recipe.fxml"));
            Parent newPane = loader.load();
            RecipeController controller = loader.getController();
            controller.initController();

            if (root.getChildren().size() > 1) {
                root.getChildren().remove(1,2);
            }
            root.getChildren().add(newPane);
        }
        event.consume();
    }
}
