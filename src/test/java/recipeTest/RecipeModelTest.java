package recipeTest;


import app.recipe.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class RecipeModelTest {
    RecipeModel model;

    @BeforeEach
    void setup() {
        RecipeCollection recipeCollection = new RecipeCollection();
        recipeCollection.addRecipe(new Recipe(FoodType.STARTER, "räkcocktail", "god till nyår", null));
        recipeCollection.addRecipe(new Recipe(FoodType.STARTER, "Gubbröra", "lätt och snabbt", "https://www.koket.se/per_morberg/forratter/fisk_och_skaldjur/per_morbergs_gubbrora/"));
        recipeCollection.addRecipe(new Recipe(FoodType.MAINCOURSE, "Lasagne", "Enkelt att göra", null));
        recipeCollection.addRecipe(new Recipe(FoodType.MAINCOURSE, "Bolognese", "Barnmat", "https://www.arla.se/recept/klassisk-bolognese/"));
        recipeCollection.addRecipe(new Recipe(FoodType.MAINCOURSE, "Pizza", "Fest", null));
        recipeCollection.addRecipe(new Recipe(FoodType.DESERT, "Panacotta", "supergod", null));
        recipeCollection.addRecipe(new Recipe(FoodType.DESERT, "Rabarberpaj", "Sommarpaj", "https://www.arla.se/recept/rabarberpaj-med-smuldeg/"));
        recipeCollection.addRecipe(new Recipe(FoodType.DESERT, "Fruktsallad", "Enkelt men nyttigt och gott", null));
        recipeCollection.addRecipe(new Recipe(FoodType.DRINK, "Pinacolada", "Fest dryck", null));
        recipeCollection.addRecipe(new Recipe(FoodType.DRINK, "Milkshake", "Shake it up", null));

        try {
            model = new RecipeModel(recipeCollection);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Test
    void addTest() {
        model.add(new Recipe(FoodType.MAINCOURSE, "Pasta", "", null));
        assertEquals(11, model.update().size());
    }

    @Test
    void removeTest() {
        model.selectRecipe("Pizza");
        model.remove();
        assertEquals(9, model.update().size());

    }
//
//    @Test
//    void filterOnTypeTest() {
//        model.filterOnType(FoodType.MAINCOURSE);
//        assertEquals(3, model.update().size());
//
//        model.filterOnType(FoodType.STARTER);
//        assertEquals(2,model.update().size());
//    }

    @Test
    void filterOnNameTest() {
        model.filterOnName("Pizza");
        assertEquals(1, model.update().size());

        model.filterOnName("P");
        assertEquals(4, model.update().size());
    }

//    @Override
//    public void propertyChange(PropertyChangeEvent evt) {
//        recipeList = (List<Recipe>) evt.getNewValue();
//    }
}
