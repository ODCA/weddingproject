package recipeTest;


import app.recipe.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class RecipeCollectionTest {

    private RecipeCollection recipes = new RecipeCollection();

    /**
     * set up collection to contain 10 recipes. 2 starter,3 main courses, 3 desert, 2 drink
     */
    @BeforeEach
    void setup() {
        recipes.addRecipe(new Recipe(FoodType.STARTER, "räkcocktail", "god till nyår", null));
        recipes.addRecipe(new Recipe(FoodType.STARTER, "Gubbröra", "lätt och snabbt", "https://www.koket.se/per_morberg/forratter/fisk_och_skaldjur/per_morbergs_gubbrora/"));
        recipes.addRecipe(new Recipe(FoodType.MAINCOURSE, "Lasagne", "Enkelt att göra", null));
        recipes.addRecipe(new Recipe(FoodType.MAINCOURSE, "Bolognese", "Barnmat", "https://www.arla.se/recept/klassisk-bolognese/"));
        recipes.addRecipe(new Recipe(FoodType.MAINCOURSE, "Pizza", "Fest", null));
        recipes.addRecipe(new Recipe(FoodType.DESERT, "Panacotta", "supergod", null));
        recipes.addRecipe(new Recipe(FoodType.DESERT, "Rabarberpaj", "Sommarpaj", "https://www.arla.se/recept/rabarberpaj-med-smuldeg/"));
        recipes.addRecipe(new Recipe(FoodType.DESERT, "Fruktsallad", "Enkelt men nyttigt och gott", null));
        recipes.addRecipe(new Recipe(FoodType.DRINK, "Pinacolada", "Fest dryck", null));
        recipes.addRecipe(new Recipe(FoodType.DRINK, "Milkshake", "Shake it up", null));
    }

    /**
     * Test adding recipes to collection and retriving data from them
     */
    @Test
    void addRecipe() {
        recipes.addRecipe(new Recipe(FoodType.MAINCOURSE, "potatisGratäng", "Bra till större sällskap", "https://www.ica.se/recept/potatisgratang-3680/"));
        assertEquals(11, recipes.size());
    }

    /**
     * Test removal of recipes in collection
     */
    @Test
    void removeRecipe() {
        recipes.removeRecipe("räkcocktail");

        assertEquals(9, recipes.size());
    }

    /**
     * Test Recipe equals
     */
    @Test
    void recipeEquals() {
        Recipe r1 = new Recipe(FoodType.MAINCOURSE, "Pasta", "");
        Recipe r2 = new Recipe(FoodType.MAINCOURSE, "Pasta", "");

        assertTrue(r1.equals(r2));
        assertTrue(r2.equals(r1));
        assertTrue(r1.equals("pasta"));
        assertTrue(r2.equals("PASTA"));
    }
}
