package taskTest;

import app.task.User;
import app.task.UsersList;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class UsersListTest {

    private UsersList list;

    @BeforeEach
    void setup() {
        list = new UsersList();

    }

    @AfterEach
    void clear() {
        list = null;
    }
    @Test
    void addUsersTest() {
        assertTrue(list.add(new User("Olle")));
        assertEquals(1, list.size());
        assertTrue(list.add(new User("Stina")));
        assertEquals(2, list.size());

        assertNotNull(list.get("Stina"));
        assertNotNull(list.get("Olle"));
    }

    @Test
    void removeUsersTest() {
        User stina = new User("Stina");
        list.add(new User("Olle"));
        list.add(stina);

        assertNotNull(list.remove("Olle"));
        assertEquals(1, list.size());
        assertNull(list.get("Olle"));

        assertTrue(list.remove(stina));
        assertEquals(0, list.size());
        assertNull(list.get("Stina"));
    }
}
