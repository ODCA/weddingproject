package taskTest;

import app.task.TaskModel;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class TaskModelUsageTest {

    private TaskModel model;

    @BeforeEach
    void setUp() {
        model = new TaskModel();

        //Add users
        model.createUser("Olle");
        model.createUser("Stina");

        //Add tasks
         model.createTask("vacuum clean");
         model.createTask("cook food");
         model.createTask("shop food");
         model.createTask("do the laundry");
         model.createTask("dry laundry");
         model.createTask("do the Dishes");
         assertEquals(6, model.nbrTasks());
    }

    /**
     * all users have the same amount of work done before
     * and will split an EVEN amount of work
     */
    @Test
    void splitEvenNbrTaskWhenEqual() {
        assertTrue(model.addToDo("vacuum clean"));
        assertTrue(model.addToDo("cook food"));
        assertTrue(model.addToDo("shop food"));
        assertTrue(model.addToDo("do the laundry"));

        Map<String, List<String>> toDoList = model.distribute();

        assertEquals(2, toDoList.get("Olle").size());
        assertEquals(2, toDoList.get("Stina").size());
    }

    /**
     * all users have the same amount of work done before
     * and will split an UNEVEN amount of work
     */
    @Test
    void splitUnevenNbrTaskWhenEqual() {
        assertTrue(model.addToDo("vacuum clean"));
        assertTrue(model.addToDo("cook food"));
        assertTrue(model.addToDo("shop food"));

        Map<String, List<String>> toDoList = model.distribute();
        assertTrue(toDoList.get("Olle").size() > 0, "Olle got more than 0 work");
        assertTrue(toDoList.get("Olle").size() < 3, "Olle got less than 3 work");
        assertTrue(toDoList.get("Stina").size() > 0, "Olle got more than 0 work");
        assertTrue(toDoList.get("Stina").size() < 3, "Olle got less than 3 work");

    }

    /**
     * all users have NOT the same amount of work done before
     * and will split an EVEN amount of work
     */
    @Test
    void splitEvenNbrTaskWhenUnequal() {
        makeTasksUnEqual();
        assertTrue(model.addToDo("vacuum clean"));
        assertTrue(model.addToDo("cook food"));
        assertTrue(model.addToDo("shop food"));
        assertTrue(model.addToDo("do the laundry"));

        Map<String, List<String>> toDoList = model.distribute();

        assertEquals(2, toDoList.get("Olle").size());
        assertEquals(2, toDoList.get("Stina").size());

        assertFalse(toDoList.get("Olle").contains("vacuum clean"), "Should not contain vacuum clean");
        assertFalse(toDoList.get("Stina").contains("cook food"), "Should not contain cook food");

    }

    /**
     * all users have NOT the same amount of work done before
     * and will split an UNEVEN amount of work
     */
    @Test
    void splitUnevenNbrTaskWhenUnequal() {
        //not implemented
        makeTasksUnEqual();
        assertTrue(model.addToDo("cook food"));
        assertTrue(model.addToDo("shop food"));
        assertTrue(model.addToDo("do the laundry"));

        Map<String, List<String>> toDoList = model.distribute();

        assertTrue(toDoList.get("Olle").size() > 0, "Olle should have more than 0 work");
        assertTrue(toDoList.get("Stina").size() > 0, "Stina should have more than 0 work");

        assertFalse(toDoList.get("Stina").contains("cook food"), "Should not contain cook food");
    }

    /**
     * make Olle have done vacuum cleaning before
     * and Stina have done cook food before
     */

    private void makeTasksUnEqual() {
        model.addToDo("vacuum clean");
        model.addToDo("cook food");

        Map<String, List<String>> toDoList = model.distribute();

        assertEquals(1, toDoList.get("Olle").size());
        assertEquals("vacuum clean", toDoList.get("Olle").get(0));
        assertEquals(1, toDoList.get("Stina").size());
        assertEquals("cook food", toDoList.get("Stina").get(0));
    }
}
