package taskTest;

import app.task.TaskModel;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

//Basic test case
class TaskModelBasicTest {

    private TaskModel model;
    @BeforeEach
    void setUp() {
        model = new TaskModel();
    }

    @AfterEach
    void cleanup() {
        model = null;
    }

    @Test
    void addTaskTest() {
        assertEquals(model.nbrTasks(), 0);
        assertTrue(model.createTask("clean"));
        assertEquals(model.nbrTasks(),1);
    }

    @Test
    void removeTaskTest() {
        assertEquals(model.nbrTasks(), 0);
        assertTrue(model.createTask("clean"));
        assertEquals(model.nbrTasks(), 1);
        assertTrue(model.removeTask("clean"));
        assertEquals(model.nbrTasks(), 0);
    }

    @Test
    void addUsersTest() {
        assertEquals(model.nbrUsers(), 0);
        assertNotNull(model.createUser("Olle"));
        assertEquals(model.nbrUsers(), 1);
    }

    @Test
    void addUserDuplicateTest() {
        assertEquals(model.nbrUsers(), 0);
        assertNotNull(model.createUser("Olle"));
        assertEquals(model.nbrUsers(), 1);
        assertNull(model.createUser("Olle"));
        assertEquals(model.nbrUsers(), 1);
    }


    @Test
    void removeUsersTest() {
        assertEquals(model.nbrUsers(), 0);
        model.createUser("Olle");
        assertEquals(model.nbrUsers(), 1);
        assertNotNull(model.removeUser("Olle"));
        assertEquals(model.nbrUsers(), 0);
    }

    @Test
    void removeUserNotExistingTest() {
        assertEquals(model.nbrUsers(), 0);
        model.createUser("Olle");

        assertEquals(model.nbrUsers(), 1);
        assertNull(model.removeUser("Stina"));
        assertEquals(model.nbrUsers(), 1);
    }

    @Test
    void addWorkTest() {
        model.createUser("Olle");
        assertTrue(model.createTask("clean"));
        assertTrue(model.createTask("cook"));

        assertEquals(model.addWork("Olle", "clean"),1);
    }

    @Test
    void getUserWork() {
        model.createUser("Olle");
        assertTrue(model.createTask("clean"));
        assertTrue(model.createTask("cook"));

        assertEquals(1, model.addWork("Olle", "clean"));
        assertEquals(1, model.checkWork("Olle", "clean"));
    }

    @Test
    void getUsers() {
        model.createUser("Olle");
        model.createUser("Stina");
        model.createUser("Kalle");

        List<String> users = model.getUsers();
        assertEquals(3, users.size());
        assertTrue(users.contains("Olle"));
        assertTrue(users.contains("Stina"));
        assertTrue(users.contains("Kalle"));
    }
}
